// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.math.util.Units;
import swervelib.math.Matter;
import swervelib.parser.PIDFConfig;
import com.pathplanner.lib.util.PIDConstants;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

  public static final double ROBOT_MASS = (148 - 20.3) * 0.453592; // 32lbs * kg per pound
  public static final Matter CHASSIS    = new Matter(new Translation3d(0, 0, Units.inchesToMeters(8)), ROBOT_MASS);
  public static final double LOOP_TIME  = 0.13; //s, 20ms + 110ms sprk max velocity lag

  public static final class Auton
  {

    public static final PIDFConfig TranslationPID     = new PIDFConfig(0.7, 0, 0);
    public static final PIDFConfig angleAutoPID = new PIDFConfig(0.4, 0, 0.01);

    public static final double MAX_ACCELERATION = 2;
  }
public static final class Autonconstants 
{

   public static final PIDConstants TRANSLATION_PID = new PIDConstants(0.7,0,0);
   public static final PIDConstants ANGLE_PID = new PIDConstants(0.4,0,0.01); 
}

  public static final class GroundIntakeConstants {
    public static final int kGroundIntakeMotor1ID = 26;//placeholder
    public static final int kGroundIntakeMotor2ID = 27;//placeholder
 }

  public static final class Drivebase
  {

    // Hold time on motor brakes when disabled
    public static final double WHEEL_LOCK_TIME = 10; // seconds
    public static final double kMaxSpeed = 4.5;  // Max swerve speed in feet/second

    //Set slew limits
    public static final double kTranslationLimit = 0; //updated per jason on 2/10/24, prev value 1
    public static final double kStrafeLimit = 0; //updated per jason on 2/10/24, prev value 1
    public static final double kRotationLimit = 0; //updated per jason on 2/10/24, prev value 1

     // Translate to Apriltag
    public static final PIDController APRILTAG_ROTATION_PID_CONTROLLER =
        new PIDController(2, 0, 0.05);
    public static final PIDController APRILTAG_X_TRANSLATION_PID_CONTROLLER =
        new PIDController(1.5, 0, 0);
    public static final PIDController APRILTAG_Y_TRANSLATION_PID_CONTROLLER =
        new PIDController(0.7, 0, 0);

    public static final double AUTO_TRANSLATE_DEBOUNCE_SECONDS = 0.1;

    public static final double AMP_ROTATION_SETPOINT = Math.PI / 2;
  }

  public static final class LimeLightConstants {
    public static final String kLimelightName = "limelight";
    public static final int kAllAprilTagPipeline = 0;

    public static final double kAPRILTAG_X_TOLERANCE = 0.05;
    public static final double kAPRILTAG_Y_TOLERANCE = 0.05;
    public static final double kAPRILTAG_ROTATION_TOLERANCE = .025; // Radians

    // Field Element locations in LL Coordinates
    public static final double kRedAmpX = 2.0;
    public static final double kRedAmpY = 0.0;
    public static final double kRedAmpRot = 90;
  }

  public static class OperatorConstants
  {

    // Joystick Deadband
    public static final double LEFT_X_DEADBAND = 0.01;
    public static final double LEFT_Y_DEADBAND = 0.01;
    public static final double RIGHT_X_DEADBAND = 0.01;
    public static final double TURN_CONSTANT = 6; 
    public static final int kDriverControllerPort = 0;
  
  }


public static final class ClimbConstants {
  public static final double kUpperSoftLimit = 40;
  public static final double kLowerSoftLimit = 0;
  public static final int kCANIDClimbMotor = 13;
  public static final double kClimbGearRatio = (2 / 1.0); // 3:1 
  public static final int kClimbTopPosition = 35; //inches
  public static final int kClimbBottomPosition = 1; //inches
  public static final double kClimbMotorDownSpeed = -0.25;
  public static final double kClimbMotorUpSpeed = 0.70;
  public static final double kClimbP = .25;
  public static final double kClimbI = 0;
  public static final double kClimbD = 0;
  public static final double kClimbIz = 0;
  public static final double kClimbFF = 0;
  public static final double kClimbMaxOutput = 1;
  public static final double kClimbMinOutput = -1;
}
public static final class Shooter_IntakeConstants {
  public static final int kCANIDLeftShooter_IntakeMotor = 14;
  public static final int kCANIDRightShooter_IntakeMotor = 17;
  public static final int kCANIDPopupShooter_IntakeMotor = 5;
  public static final int kIntakeSensor = 1;
  public static final boolean kLeftShooter_IntakeInvert = true;
  public static final boolean kRightShooter_IntakeInvert = false;
  public static final boolean kPopupInvert = true;
  public static final double kSetLeftShooterPercentage = 1;
  public static final double kSetRightShooterPercentage = 1;
  public static final double kSetPopupPercentage = 1;
  public static final double kSetIntakePercentage = -1;
  public static final double kSetAllOffPercentage = 0;
} 

  public static final class LEDs {
    /*Led Constants */
      public static final int kPwmChannel = 1;
      public static final int kLEDBufferLength = 16 ; 
      public static final int kPurple = 215;
      public static final int kGreen = 60;
      public static final int kBlue = 125;
      public static final int kYellow = 10;
      public static final int kWhite = 255;
      public static final int kRed = 0;
      public static final int kPink = 351;
      public static final int kSat0 = 0;
      public static final int kSatDefault = 255;
      public static final int underbot_lights = 2;
      public static final int support_lights_left = 3;
      public static final int support_lights_right = 3;
   
  }
  
}
