// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.Commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants.Drivebase;
import frc.robot.Constants.LimeLightConstants;
import frc.robot.subsystems.LimelightHelpers;
import frc.robot.subsystems.swervedrive.SwerveSubsystem;

public class DriveAprilTagAngle extends Command {

  private SwerveSubsystem m_swerve;
  private boolean m_isRedAlliance;
  private double m_rotationAngle;
  private double m_tagID;
  private double m_rotationError;
  private PIDController m_rotationController;
  private DoubleSupplier m_controllerX;
  private DoubleSupplier m_controllerY;

  private double m_TestAngle;
  

  /** Creates a new DriveAprilTagAngle. */
  public DriveAprilTagAngle(SwerveSubsystem p_swerve, DoubleSupplier translationX, DoubleSupplier translationY) {
    m_swerve = p_swerve;
    m_controllerX = translationX;
    m_controllerY = translationY;
    m_rotationController = Drivebase.APRILTAG_ROTATION_PID_CONTROLLER;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_swerve);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    //Set up rotational PID
    m_rotationController.enableContinuousInput(-Math.PI, Math.PI);
    m_rotationController.reset();
    
    //Get Alliance color
    var Alliance = DriverStation.getAlliance();

    if(Alliance.get() == DriverStation.Alliance.Red)
      m_isRedAlliance = true;
    else
      m_isRedAlliance = false;

    //Set the LL Pipeline to target all Apriltags
    LimelightHelpers.setPipelineIndex(LimeLightConstants.kLimelightName, LimeLightConstants.kAllAprilTagPipeline);

    // If LL has a target
    // if(LimelightHelpers.getTV(LimeLightConstants.kLimelightName))
    //   m_tagID = LimelightHelpers.getFiducialID(LimeLightConstants.kLimelightName);
    // else {
    //   // TODO: Make LEDs blink RED
    //   return;
    //}

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    
    //Set rotation angle based on alliance and Apriltag ID
    // if(m_isRedAlliance && (m_tagID == 9 || m_tagID == 10))
    //   m_rotationAngle = LimeLightConstants.kRedSourceRot;
    // else if (m_isRedAlliance && (m_tagID == 5))
    //   m_rotationAngle = LimeLightConstants.kRedAmpRot;
    // else if (!m_isRedAlliance && (m_tagID == 1 || m_tagID == 2))
    //   m_rotationAngle = LimeLightConstants.kBlueSourceRot;
    // else if (!m_isRedAlliance && (m_tagID == 6))
    //   m_rotationAngle = LimeLightConstants.kBlueAmpRot;
    // else
    //   return; //Replace with Speaker rotation code

    //Set rotational setpoint for PID
    // TEST -> TODO: replace with m_rotationAngle
    m_TestAngle = 60;
    m_rotationController.setSetpoint(Math.toRadians(m_TestAngle));  //***Not sure if this can be done in execute
    
    //Get current rotation
    m_rotationError = m_swerve.getPose().getRotation().getRadians();

    m_swerve.drive(new Translation2d((Math.pow(m_controllerY.getAsDouble(), 3) * m_swerve.getMaxVelocity()),
      Math.pow(m_controllerX.getAsDouble(), 3) * m_swerve.getMaxVelocity()),
      m_rotationController.calculate(m_rotationError), true);

      System.out.println("ControllerX: " + String.valueOf(m_controllerX.getAsDouble()));
      System.out.println("XSpeed: " + String.valueOf(Math.pow(m_controllerX.getAsDouble(), 3) * m_swerve.getMaxVelocity()));
      System.out.println("YSpeed: " + String.valueOf(Math.pow(m_controllerY.getAsDouble(), 3) * m_swerve.getMaxVelocity()));
     
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
