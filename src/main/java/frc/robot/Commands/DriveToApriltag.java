// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.Commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.filter.Debouncer.DebounceType;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.Constants.Drivebase;
import frc.robot.Constants.LimeLightConstants;
import frc.robot.Robot;
import frc.robot.subsystems.LimelightHelpers;
import frc.robot.subsystems.swervedrive.SwerveSubsystem;

public class DriveToApriltag extends Command {

  private double m_rotationGoal;
  private int m_pipelineIndex;
  private Debouncer m_canSeePieceDebouncer;
  private SwerveSubsystem m_swerve;

  private PIDController m_xController;
  private PIDController m_yController;
  private PIDController m_rotationController;

  private double m_ty;
  private double m_tx;
  private double m_rotationError;
  private double m_txTolerance;

  /** Creates a new DriveToApriltag. */
  public DriveToApriltag(SwerveSubsystem p_swerve, int p_pipelineIndex) {
    m_pipelineIndex = p_pipelineIndex;
    m_swerve = p_swerve;

    m_xController = Drivebase.APRILTAG_X_TRANSLATION_PID_CONTROLLER;
    m_yController = Drivebase.APRILTAG_Y_TRANSLATION_PID_CONTROLLER;
    m_rotationController = Drivebase.APRILTAG_ROTATION_PID_CONTROLLER;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_swerve);

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    LimelightHelpers.setPipelineIndex(LimeLightConstants.kLimelightName, m_pipelineIndex);

    // reset pids
    m_xController.setSetpoint(LimeLightConstants.kRedAmpX);
    m_xController.reset();
    
    m_yController.setSetpoint(LimeLightConstants.kRedAmpY);
    m_yController.reset();

    m_rotationController.enableContinuousInput(-Math.PI, Math.PI);
    m_rotationController.setSetpoint(Math.toRadians(LimeLightConstants.kRedAmpRot));
    m_rotationController.reset();

    m_canSeePieceDebouncer = new Debouncer(Drivebase.AUTO_TRANSLATE_DEBOUNCE_SECONDS, DebounceType.kFalling);

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    //get vision reading from LL if target is in view
    // if (LimelightHelpers.getTV(LimeLightConstants.kLimelightName)) {
    //   System.out.println("Adding vision reading");
    //   m_swerve.addRealVisionReading();
    // }

    
    m_tx = m_swerve.getPose().getX();
    m_ty = m_swerve.getPose().getY();
    m_rotationError = m_swerve.getPose().getRotation().getRadians();

    System.out.println();
    System.out.println();
    System.out.println();
    //System.out.println("Xset: " + String.valueOf(LimeLightConstants.kRedAmpX));
    //SmartDashboard.putNumber("SetPointX: ", LimeLightConstants.kRedAmpX);
    ///System.out.println("Yset: " + String.valueOf(LimeLightConstants.kRedAmpY));
    System.out.println("RotationSet: " + String.valueOf(LimeLightConstants.kRedAmpRot));
    System.out.println();
    //System.out.println("tx = " + String.valueOf(m_tx));
    //SmartDashboard.putNumber("m_tx (swerveX): ", m_tx);
    //System.out.println("ty = " + String.valueOf(m_ty));
    System.out.println("Rotation = " + String.valueOf(Math.toDegrees(m_rotationError)));
    System.out.println();

    // Increase tx tolerance when close to target since tx is more sensitive at
    // shorter distances
    m_txTolerance = LimeLightConstants.kAPRILTAG_X_TOLERANCE;
    if (isWithinTolerance(m_ty, LimeLightConstants.kRedAmpY, 4)) {
      m_txTolerance *= 2;
    }

    if (isWithinTolerance(m_rotationError, 0, LimeLightConstants.kAPRILTAG_ROTATION_TOLERANCE)) {
      m_rotationError = 0;
    }

    // Reduce tx based on how far off our rotation is so the x controller doesn't
    // over compensate
    // m_tx -= Rotation2d.fromRadians(m_rotationError).getDegrees();
    // if (isWithinTolerance(m_tx, 0, m_txTolerance)) {
    //   m_tx = 0;
    // }
    if (isWithinTolerance(m_tx, LimeLightConstants.kRedAmpX, m_txTolerance)) {
      m_tx = LimeLightConstants.kRedAmpX;
    }
    if (isWithinTolerance(m_ty, LimeLightConstants.kRedAmpY, LimeLightConstants.kAPRILTAG_Y_TOLERANCE)) {
      m_ty = LimeLightConstants.kRedAmpY;
    }

    m_swerve.drive(new Translation2d(m_xController.calculate(m_tx), m_yController.calculate(m_ty)), 
      m_rotationController.calculate(m_rotationError), true);

      // m_swerve.drive(new Translation2d(0, 0), 
      // m_rotationController.calculate(m_rotationError), true);

      //System.out.println("PIDX: " + String.valueOf(m_xController.calculate(m_tx)));
      //System.out.println("PIDY: " + String.valueOf(m_yController.calculate(m_ty)));
      System.out.println("PIDRot: " + String.valueOf(m_rotationController.calculate(m_rotationError)));
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    System.out.println("DrivetoApriltag Interupted");
    m_swerve.drive(new Translation2d(0, 0), 0, true);
  }

  public static boolean isWithinTolerance(
      double currentValue, double targetValue, double tolerance) {
    return Math.abs(currentValue - targetValue) <= tolerance;
  }

}
