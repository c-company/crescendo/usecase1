// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import frc.robot.Constants.LimeLightConstants;
import frc.robot.Constants.OperatorConstants;
import frc.robot.Commands.swervedrive.drivebase.AbsoluteDrive;
import frc.robot.Commands.swervedrive.drivebase.AbsoluteDriveAdv;
import frc.robot.Commands.swervedrive.drivebase.AbsoluteFieldDrive;
import frc.robot.subsystems.swervedrive.SwerveSubsystem;
import edu.wpi.first.wpilibj.Servo;
import java.io.File;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.Commands.*;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.CommandJoystick;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import frc.robot.Commands.*;


/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */


public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  
 // private final ExampleSubsystem m_exampleSubsystem = new ExampleSubsystem(); 

 private final SwerveSubsystem m_drivebase = new SwerveSubsystem(new File(Filesystem.getDeployDirectory(),
 "swerve"));

// CommandJoystick driverController   = new CommandJoystick(3);//(OperatorConstants.DRIVER_CONTROLLER_PORT);
XboxController m_driverXbox = new XboxController(0);
 // set up autos for smart dashboard
private final SendableChooser<Command> m_autoChooser;
 
CommandXboxController m_manipulator = new CommandXboxController(1);
  /** The container for the robot. Contains subsystems, OI devices, and commands. */
public RobotContainer(AddressableLED p_LED, AddressableLEDBuffer p_LEDBuffer) {
    
    // Configure the trigger bindings
    configureBindings(); 
    
    AbsoluteDriveAdv closedAbsoluteDriveAdv = new AbsoluteDriveAdv(m_drivebase,
    () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY()),
                                 OperatorConstants.LEFT_Y_DEADBAND),
    () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX()),
                                 OperatorConstants.LEFT_X_DEADBAND),
    () -> MathUtil.applyDeadband(-(m_driverXbox.getRightX()),
                                 OperatorConstants.RIGHT_X_DEADBAND),
    m_driverXbox::getYButtonPressed,
    m_driverXbox::getAButtonPressed,
    m_driverXbox::getXButtonPressed,
    m_driverXbox::getBButtonPressed);

// Applies deadbands and inverts controls because joysticks
// are back-right positive while robot
// controls are front-left positive
// left stick controls translation
// right stick controls the desired angle NOT angular rotation
Command driveFieldOrientedDirectAngle = m_drivebase.driveCommand(
() -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY()), OperatorConstants.LEFT_Y_DEADBAND),
() -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX()), OperatorConstants.LEFT_X_DEADBAND),
() -> -(m_driverXbox.getRightX()),
() -> -(m_driverXbox.getRightY()));

// Applies deadbands and inverts controls because joysticks
// are back-right positive while robot
// controls are front-left positive
// left stick controls translation
// right stick controls the angular velocity of the robot
Command driveFieldOrientedAnglularVelocity = m_drivebase.driveCommand(
() -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY()), OperatorConstants.LEFT_Y_DEADBAND),
() -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX()), OperatorConstants.LEFT_X_DEADBAND),
() -> -(m_driverXbox.getRightX()));

Command driveFieldOrientedDirectAngleSim = m_drivebase.simDriveCommand(
() -> MathUtil.applyDeadband(m_driverXbox.getLeftY(), OperatorConstants.LEFT_Y_DEADBAND),
() -> MathUtil.applyDeadband(m_driverXbox.getLeftX(), OperatorConstants.LEFT_X_DEADBAND),
() -> m_driverXbox.getRawAxis(2));

//System.out.println("we are in robot container");
//m_LED.setDefaultCommand(new LEDDefaultCommand(m_LED, m_RainbowSubsystem)); 
      
m_drivebase.setDefaultCommand(
!RobotBase.isSimulation() ? driveFieldOrientedAnglularVelocity: driveFieldOrientedDirectAngleSim);
//!RobotBase.isSimulation() ? closedAbsoluteDriveAdv : driveFieldOrientedDirectAngleSim);

// Build an auto chooser. This will use Commands.none() as the default option.
    m_autoChooser = AutoBuilder.buildAutoChooser();
   SmartDashboard.putData("Auto Chooser", m_autoChooser);
   
}
  
  

  /**
   * Use this method to define your trigger->command mappings. Triggers can be created via the
   * {@link Trigger#Trigger(java.util.function.BooleanSupplier)} constructor with an arbitrary predicate, or via the
   * named factories in {@link edu.wpi.first.wpilibj2.command.button.CommandGenericHID}'s subclasses for
   * {@link CommandXboxController Xbox}/{@link edu.wpi.first.wpilibj2.command.button.CommandPS4Controller PS4}
   * controllers or {@link edu.wpi.first.wpilibj2.command.button.CommandJoystick Flight joysticks}.
   */

  /**
   * Use this method to define your trigger->command mappings. Triggers can be created via the
   * {@link Trigger#Trigger(java.util.function.BooleanSupplier)} constructor with an arbitrary
   * predicate, or via the named factories in {@link
   * edu.wpi.first.wpilibj2.command.button.CommandGenericHID}'s subclasses for {@link
   * CommandXboxController Xbox}/{@link edu.wpi.first.wpilibj2.command.button.CommandPS4Controller
   * PS4} controllers or {@link edu.wpi.first.wpilibj2.command.button.CommandJoystick Flight
   * joysticks}.
   */
  private void configureBindings() {
   
    /*** DRIVER Controller ***/

    // A: Zero Gyro
    new JoystickButton(m_driverXbox, 1).onTrue((new InstantCommand(m_drivebase::zeroGyro)));
    
    // new JoystickButton(m_driverXbox, 2).onTrue(new DriveAprilTagAngle(m_drivebase, 
    //   () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX()), OperatorConstants.LEFT_X_DEADBAND),
    //   () -> MathUtil.applyDeadband(m_driverXbox.getLeftY(), OperatorConstants.LEFT_Y_DEADBAND)));

    // C: 
    //new JoystickButton(m_driverXbox, 3).onTrue(new InstantCommand(m_drivebase::addFakeVisionReading));
    //new JoystickButton(m_driverXbox, 1).whileTrue(new DriveToApriltag(m_drivebase, LimeLightConstants.kRedAmpY, LimeLightConstants.kRedAmpRot, 0));
    
  }
 
  public Command getAutonomousCommand()
  {
    return m_autoChooser.getSelected();
  }


  public void setMotorBrake(boolean brake)
  {
    m_drivebase.setMotorBrake(brake);
  }




  public void setDriveMode()
  {
    //drivebase.setDefaultCommand();
  }


  
  /*public Command getAutonomousCommand() {
    // An example command will be run in autonomous
    return Autos.exampleAuto(m_exampleSubsystem);
  }*/
}
 
